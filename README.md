### HSE Cloud Systems, Task 2

## Local Deployment
1. Git clone the project and simply run `docker-compose up`
2. Send POST request at `http://localhost:8080/api/action?value=x`, where X - your input value 

## Kubernetess Deployment
1. API Endpoint available at `https://cs-task-2.k8s.dline-media.com`
2. Scalled to 20 replicas for laravela-app, 1 replica for regis & nginx
3. Redis has not Persistent Volume for easy storage reset